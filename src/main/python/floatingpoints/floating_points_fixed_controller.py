import sys, multiprocessing, time
from multiprocessing.sharedctypes import Array
from random import randint

from PyQt5.Qt import *
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import *


from floatingpoints import floating_points_resizeable_view


class FloatingPointController(QtWidgets.QMainWindow):
    """
    MVC pattern: Creates a controller according to the mvc pattern.

    :ivar safe_close: Safe closing of initiated points
    :ivar point_positions: List of points as a reference to a Integer-Array of Shared memory
    :ivar main_form: Qt Form
    """

    def __init__(self):
        super().__init__()
        # Get Ui and set it up
        self.main = floating_points_resizeable_view.Ui_MainWindow()
        self.main.setupUi(self)
        # Set up a minimum height, so points that are 50px large are still drawn
        self.setMinimumHeight(160)
        # Create list, that stores the points
        self.points = []
        # "Listener" that launches the method new_point, when button pushButton_2
        # is pressed
        self.main.pushButton_2.clicked.connect(self.new_point)
        # "Listener" that launches the method remove_point when button pushButton
        # is pressed
        self.main.pushButton.clicked.connect(self.remove_point)
        # List that stores the processes
        self.pr = []
        # Is later need in refres_loop for constantly updating the gui
        self.refresher = QtCore.QTimer()
        # Listener that triggers the method closeEvent, when the user closes
        # the program windows
        self.end = QAction("Quit", self)
        self.end.triggered.connect(self.closeEvent)
        # List that stores all possible colors for points
        self.colors = [Qt.black, Qt.darkRed, Qt.darkBlue, Qt.darkGreen]
        # Create a list that stores the width and the height of the window as a shared memory value
        self.dimensions = []
        self.dimensions.append(multiprocessing.Value('i', lock=False))
        self.dimensions.append(multiprocessing.Value('i', lock=False))
        self.dimensions[0].value = self.width()
        self.dimensions[1].value = self.height()-55

    def new_point(self):
        """
        Add a new point
        """
        # Create a new array from class multiprocessing
        self.points.append(Array('i', 5))
        self.points[-1][0] = randint(0, self.dimensions[0].value - self.point_size())   # Random X-axis start point
        self.points[-1][1] = randint(0, self.dimensions[1].value - self.point_size())   # Random Y-axis start point
        self.points[-1][2] = 1                      # Flag for living_point, 1 -> Lives, 0 -> Stop
        self.points[-1][3] = self.color_index()     # Color Selection
        self.points[-1][4] = self.point_size()      # Size Selection

        # Starts the Process, which moves the "point"
        self.pr.append(multiprocessing.Process(
            target=living_point, args=(
                self.points[-1], randint(1, 9), randint(1, 9), self.dimensions[0], self.dimensions[1])))
        self.pr[-1].start()

    def remove_point(self):
        """
        Remove the last initiated point
        """
        if len(self.points):
            self.points[-1][2] = 0
            # sleep shortly, so that join living_points is most likely finished and fast to join and close
            QThread.sleep(0.05)
            self.pr[-1].join()
            self.pr[-1].close()
            del self.pr[-1]
            del self.points[-1]

    def paintEvent(self, event):
        """ React to a paint event

        :param event: QPaintEvent, but we ignore the value and repaint the whole qwidget
        """
        qt_painter = QPainter(self)
        self.draw_points(qt_painter)
        qt_painter.end()

    def draw_points(self, qt_painter):
        """
        Drawing all the Points from the point_positions List in their colours and sizes

        :param qt_painter: Painter Object for Widget painting
        """
        # Draw all points in the list points
        for pt in self.points:
            # Get color from point and set pen
            color = self.colors[pt[3]]
            qt_painter.setPen(color)
            # Draw the point
            qt_painter.drawEllipse(pt[0], pt[1], pt[4], pt[4])

    def closeEvent(self, event):
        """
        Overriding QWidget method for implementing the close event

        Closing all running process from point_positions
        Setting also safe_close to True, which closes the application.

        :param event: Event object which contains the event parameters
        """
        # Stop QTimer from refresh_loop
        self.refresher.stop()

        # Stop all living points
        for pt in self.points:
            pt[2] = 0

        # Close all process objects and releasing their resources
        for p in self.pr:
            p.join()
            p.close()

    def resizeEvent(self, event):
        """
        Resize Event is triggered, when the user changes the window dimensions
        The method updates the list dimensions, that stores the current window width and height
        :param event: Event Informations
        """
        # Update the dimensions list
        self.dimensions[0].value = self.width()
        self.dimensions[1].value = self.height()-55
        super()

    def refresh_loop(self):
        """
        Refreshing the GUI every .025 seconds and processing any QApplication Events
        """
        # Start the refresher, so that self.update is called every .025 to redraw the UI
        self.refresher.setInterval(25)
        self.refresher.timeout.connect(self.update)
        self.refresher.start()

    def color_index(self):
        """
        Return the current selected color in the combobox as the index number of the list colors:
        black - 0, red - 1, blue - 2, green - 3
        :return: Selected color as the index number of the list colors
        """
        # Get selected item as string
        value = self.main.comboBox.currentText()
        index = 0
        if value == "Rot":
            index = 1
        elif value == "Blau":
            index = 2
        elif value == "Grün":
            index = 3
        return index

    def point_size(self):
        """
        Checks the user input in the lineEdit field and returns it if its a number.
        If the number is lower than 3, it is set to 3.
        If the number is greater than 50, it is set to 50.
        If no number is given, the number 3 is used.
        :return: An number that is representative of the criteria above
        """
        text = self.main.lineEdit.text()
        if text.isdigit():
            ptsize = int(text)
            if ptsize < 3:
                ptsize = 3
                self.main.lineEdit.setText("3")
            elif ptsize > 50:
                ptsize = 50
                self.main.lineEdit.setText("50")
        else:
            ptsize = 3
            self.main.lineEdit.setText("3")
        return ptsize


def living_point(point_position, vx, vy, window_width, window_height):
        """
        Method for concurrent processing of 2D-points

        :param point_position: Reference to Integer-Array as Shared memory
        :param vx: speed in x-axis
        :param vy: speed in y-axis
        :param window_width: Current window width, stored in an shared memory value
        :param window_height: Current window height, stored in an shared memory value

        """
        while point_position[2]:
            # The first two conditions check, if the point is out of boundaries (as a result of resizing)
            # If it is, it is slowly positioned back, until it is back in the visible window area
            if (point_position[0]+point_position[4]) > window_width.value:
                point_position[0] -= 10
            elif (point_position[1]+point_position[4]) > window_height.value:
                point_position[1] -= 10
            else:
                dx = int((point_position[0] + vx) / (window_width.value - point_position[4]))
                dy = int((point_position[1] + vy) / (window_height.value - point_position[4]))
                dx2 = point_position[0] + vx < 0
                dy2 = point_position[1] + vy < 0
                point_position[0] = point_position[0] + vx - dx * ((point_position[0] + vx) % (
                        window_width.value - point_position[4])) - dx2 * (
                            point_position[0] + vx)
                point_position[1] = point_position[1] + vy - dy * ((point_position[1] + vy) % (
                        window_height.value - point_position[4])) - dy2 * (
                            point_position[1] + vy)
                vx = int((-2 * (dx - 0.5)) * int(-2 * (dx2 - 0.5)) * vx)
                vy = int((-2 * (dy - 0.5)) * int(-2 * (dy2 - 0.5)) * vy)
                time.sleep(0.05)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    c = FloatingPointController()
    c.show()
    c.refresh_loop()
    sys.exit(app.exec_())
