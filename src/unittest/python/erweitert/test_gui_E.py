#from floatingpoints.floating_points_resizeable_controller import FloatingPointController
from floatingpoints.floating_points_fixed_controller import FloatingPointController

# https://pytest-qt.readthedocs.io/en/latest/index.html
import pytest
from PyQt5 import QtCore
from pytestqt.qt_compat import qt_api

import unittest, sys
from PyQt5 import QtGui, QtWidgets, QtTest


def test_isWidgetType(qtbot):
	# confirms FloatingPointController inherits from QWidget: class FloatingPointController(QWidget):
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.isWidgetType() ", window.isWidgetType()) # gives True
	assert window.isWidgetType()

	window.close()


def test_has_myForm(qtbot):
	# confirms FloatingPointController has an item main_form
	# which represents the form in the FloatingPointController
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	mf = [i for i in dir(window) if i == "myForm"]
	print("window ", mf)
	assert mf == ["myForm"]

	window.close()


def test_has_all_components(qtbot):
	# confirms if FloatingPointController's main_form has all necessary components
	# all items are added to the verticalLayout which is the layout of choice of the form itself
	# a vertical spacer item is also in verticalLayout but not queryable here
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	a = set()
	a.update([i for i in dir(window.myForm)])
	viewSet0 = {"button_new", "button_remove", "gridLayout", "gridLayout_3", "comboBox", "spinBox"}
	assert viewSet0.issubset(a)

	window.close()

	a = set()
	a.update([i for i in dir(window.myForm.gridLayout)])
	viewSet0 = {"spacerItem"}
	assert viewSet0.issubset(a)

	window.close()


def test_window_layout(qtbot):
	# checks if the form itself has the layout parameter vertical Layout QVBoxLayout checked
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.layout", window.layout())
	assert type(window.layout()) == QtWidgets.QGridLayout

	window.close()


def test_resizeable_0(qtbot):
	#checks if form is really set on fixed size
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	# test resizeable 
	assert window.myForm.gridLayout.sizeConstraint() == QtWidgets.QLayout.SetDefaultConstraint  

	print("gridLayout.layoutSizeConstraint() ", window.myForm.gridLayout.sizeConstraint()) # 0
	assert window.myForm.gridLayout.sizeConstraint() == 0 
	# 3 means verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
	assert 0 == QtWidgets.QLayout.SetDefaultConstraint

	window.close()


def test_resizeable_1(qtbot):
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	window.resize(405, 271)
	assert window.size() == QtCore.QSize(405, 271) 

	print("window.size before ", window.size())
	window.resize(500, 400)
	print("window.size after ", window.size())
	assert window.size() == QtCore.QSize(500, 400) 

	window.close()


def test_size(qtbot):
	# checks size of the window 
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.geometry ", window.geometry())
	print("window.size ", window.size())
	assert window.size() == QtCore.QSize(405, 271) 

	window.close()


def test_horizontalLayout(qtbot):
	# checks if grid layout is part of, or added to, the grid layout of main form
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.myForm.gridLayout.children ", type(window.myForm.gridLayout.children()))
	assert type(window.myForm.gridLayout.children()[0]) == QtWidgets.QGridLayout

	window.close()


def test_gridLayout(qtbot):
	# checks that grid layout covers the whole window size,
	# which means grid layout is indeed an adjective of the form itself
	# selected  by right click on the form -> layout
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.myForm.gridLayout.geometry() ", window.myForm.gridLayout.geometry())
	assert window.myForm.gridLayout.geometry() == QtCore.QRect(0, 0, 405, 271)

	window.close()


def test_button_size(qtbot):
	# checks button_new to be on bottom left of the form
	# button is added as a widget to grid layout of the form
	# checks button_remove to be on bottom right of the form
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.myForm.button_new.geometry() ", window.myForm.button_new.geometry())
	assert window.myForm.button_new.geometry() == QtCore.QRect(11, 237, 189, 23)

	print("window.myForm.button_remove.geometry() ", window.myForm.button_remove.geometry())
	assert window.myForm.button_remove.geometry() == QtCore.QRect(206, 237, 188, 23)

	print("window.myForm.comboBox.geometry() ", window.myForm.comboBox.geometry())
	assert window.myForm.comboBox.geometry() == QtCore.QRect(206, 211, 188, 20)

	print("window.myForm.spinBox.geometry() ", window.myForm.spinBox.geometry())
	assert window.myForm.spinBox.geometry() == QtCore.QRect(11, 211, 189, 20)

	window.close()
