#from floatingpoints.from floating_points_resizeable_controller import FloatingPointController
from floatingpoints.floating_points_fixed_controller import FloatingPointController

# https://pytest-qt.readthedocs.io/en/latest/index.html
import pytest
from PyQt5 import QtCore
from pytestqt.qt_compat import qt_api


def test_start_without_points(qtbot):
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    assert len(window.q) == 0

    window.close()


def test_one_point(qtbot):

    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)

    assert len(window.q) == 1
    window.close()


def test_one_point_after_resize(qtbot):
    # TODO check if the new height*width is used for new point
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)

    window.resize(405, 271)
    assert window.size() == QtCore.QSize(405, 271) 

    print("window.size before ", window.size())
    window.resize(500, 400)
    print("window.size after ", window.size())
    assert window.size() == QtCore.QSize(500, 400)

    assert len(window.q) == 1
    window.close()


def test_two_points(qtbot):

    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)

    assert len(window.q) == 2
    window.close()


def test_one_point_removed(qtbot):

    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.myForm.button_remove, QtCore.Qt.LeftButton)

    assert len(window.q) == 1

    window.close()


def test_remove_non_existing_points(qtbot):

    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    qtbot.mouseClick(window.myForm.button_remove, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.myForm.button_remove, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.myForm.button_remove, QtCore.Qt.LeftButton)

    assert len(window.q) == 0

    window.close()


def test_random_position_x(qtbot):
   # minimum 5 points, with time difference, check positions -> absolute (x,y)
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)

    x = []
    for p in window.q:
        point_position = p.x.value
        x.append(point_position)
        print("positions ", point_position)

    print("x ", x)

    dx = []
    nFailed = 0
    for i in range(len(x)-1):
        if x[i+1] == x[i]: #checks if points are not on the same x
            nFailed += 1
        dx.append(x[i+1] -  x[i])
    assert nFailed <= 1 #one failure is allowed

    print("delta x ", dx)

    nFailed = 0
    for i in range(len(dx)-1):
        if dx[i+1] == dx[i]: #checks if points do not have the same dx steps
            nFailed += 1
    assert nFailed <= 1 #one failure is allowed

    window.close()


def test_random_position_y(qtbot):
    # minimum 5 points, with time difference, check positions -> absolute (x,y)
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)

    y = []
    for p in window.q:
        point_position = p.y.value
        y.append(point_position)

    print("y ", y)

    dy = []
    nFailed = 0
    for i in range(len(y)-1):
        if y[i+1] == y[i]: #checks if points are not on the same y
            nFailed += 1
        dy.append(y[i+1] -  y[i])
    assert nFailed <= 1 #one failure is allowed

    print("delta y ", dy)

    nFailed = 0
    for i in range(len(dy)-1):
        if dy[i+1] == dy[i]: #checks if points do not have the same dy steps
            nFailed += 1
    assert nFailed <= 1 #one failure is allowed

    window.close()


def test_random_velocity_x(qtbot):
    # minimum 5 points, with time difference, check positions -> velocity
    # minimum 5 points, with time difference, check positions -> absolute (x,y)
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)

    x = []
    for p in window.q:
        point_speed = p.vx.value
        x.append(point_speed)

    print("y ", x)

    dx = []
    nFailed = 0
    for i in range(len(y)-1):
        if x[i+1] == x[i]: #checks if speeds are not on the same y
            nFailed += 1
        dx.append(x[i+1] -  x[i])
    assert nFailed <= 2 #two failure is allowed


def test_random_velocity_y(qtbot):
    # minimum 5 points, with time difference, check positions -> velocity
    # minimum 5 points, with time difference, check positions -> absolute (x,y)
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.myForm.button_new, QtCore.Qt.LeftButton)

    y = []
    for p in window.q:
        point_speed = p.vy.value
        y.append(point_speed)

    print("y ", y)

    dy = []
    nFailed = 0
    for i in range(len(y)-1):
        if y[i+1] == y[i]: #checks if speeds are not on the same y
            nFailed += 1
        dy.append(y[i+1] -  y[i])
    assert nFailed <= 2 #two failure is allowed

