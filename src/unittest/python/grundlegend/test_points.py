#from floatingpoints.\
from floatingpoints.floating_points_fixed_controller import FloatingPointController

# https://pytest-qt.readthedocs.io/en/latest/index.html
import pytest
from PyQt5 import QtCore
from pytestqt.qt_compat import qt_api
import time

def test_start_without_points(qtbot):
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    assert len(window.point_positions) == 0
    window.close()


def test_one_point(qtbot):
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)

    assert len(window.point_positions) == 1
    window.close()


def test_two_points(qtbot):
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)

    assert len(window.point_positions) == 2
    window.close()


def test_one_point_removed(qtbot):
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_remove, QtCore.Qt.LeftButton)

    assert len(window.point_positions) == 1
    window.close()


def test_remove_non_existing_points(qtbot):
    # checks survival of pushing "remove point" button 5 times without any points present
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.main_form.button_remove, QtCore.Qt.LeftButton)

    assert len(window.point_positions) == 0
    window.close()


def test_random_position_x(qtbot):
    # minimum 5 points, with time difference, check positions -> absolute (x,y)
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)

    x = []
    for i in range(len(window.point_positions)):
        point_position = window.point_positions[i]
        x.append(point_position[0])
        print("positions ", point_position[0], (window.point_positions[i])[0])

    print("x ", x)

    dx = []
    nFailed = 0
    for i in range(len(x)-1):
        if x[i+1] == x[i]: #checks if points are not on the same x
            nFailed += 1
        dx.append(x[i+1] -  x[i])
    assert nFailed <= 1 #one failure is allowed

    print("delta x ", dx)

    nFailed = 0
    for i in range(len(dx)-1):
        if dx[i+1] == dx[i]: #checks if points do not have the same dx steps
            nFailed += 1
    assert nFailed <= 1 #one failure is allowed

    window.close()


def test_random_position_y(qtbot):
    # minimum 5 points, with time difference, check positions -> absolute (x,y)
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)

    y = []
    for i in range(len(window.point_positions)):
        point_position = window.point_positions[i]
        y.append(point_position[1])

    print("y ", y)

    dy = []
    nFailed = 0
    for i in range(len(y)-1):
        if y[i+1] == y[i]: #checks if points are not on the same y
            nFailed += 1
        dy.append(y[i+1] -  y[i])
    assert nFailed <= 1 #one failure is allowed

    print("delta y ", dy)

    nFailed = 0
    for i in range(len(dy)-1):
        if dy[i+1] == dy[i]: #checks if points do not have the same dy steps
            nFailed += 1
    assert nFailed <= 1 #one failure is allowed

    window.close()


def test_random_velocity_x(qtbot):
    # minimum 5 points, with time difference, check positions -> velocity
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)

    x0 = []
    for i in range(len(window.point_positions)):
        point_position = window.point_positions[i]
        x0.append(point_position[0])
        print("positions 0 ", point_position[0], (window.point_positions[i])[0])

    print("x0 ", x0)

    time.sleep(1.025)

    x1 = []
    for i in range(len(window.point_positions)):
        point_position = window.point_positions[i]
        x1.append(point_position[0])
        print("positions 1 ", point_position[0], (window.point_positions[i])[0])

    print("x1 ", x1)

    dx = [] # speed over x
    for i in range(len(x0)):
        dx.append(x1[i] -  x0[i])

    print("delta dx ", dx)

    nFailed = 0
    for i in range(len(dx)-1):
        if dx[i+1] == dx[i]: #checks if points do not have the same x speed
            nFailed += 1
    assert nFailed <= 1 #one failure is allowed

    window.close()

def test_random_velocity_y(qtbot):
    # minimum 5 points, with time difference, check positions -> velocity
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    for i in range(5):
        qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)

    y0 = []
    for i in range(len(window.point_positions)):
        point_position = window.point_positions[i]
        y0.append(point_position[1])
        print("positions 0 ", point_position[0], (window.point_positions[i])[0])

    print("y0 ", y0)

    time.sleep(1.025)

    y1 = []
    for i in range(len(window.point_positions)):
        point_position = window.point_positions[i]
        y1.append(point_position[1])

    print("y1 ", y1)

    dy = [] # speed over y
    for i in range(len(y0)):
        dy.append(y1[i] -  y0[i])

    print("delta dy ", dy)

    nFailed = 0
    for i in range(len(dy)-1):
        if dy[i+1] == dy[i]: #checks if points do not have the same y speed
            nFailed += 1
    assert nFailed <= 1 #one failure is allowed

    window.close()


def test_maximum_points(qtbot):
    # memory overflow if too many points are initiated
    # checks if there is a max number of points to  be created below 30 points
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    pointsToFindMax = 30
    for i in range (pointsToFindMax):
        qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)

    max_points = len(window.point_positions)
    # must have stopped to create new points before pointsToCheck
    assert max_points < pointsToFindMax

    window.close()


