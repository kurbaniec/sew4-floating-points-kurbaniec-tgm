# from floatingpoints.
#from floating_points_fixed_controller import FloatingPointController
from floating_points_fixed_controller import FloatingPointController

# https://pytest-qt.readthedocs.io/en/latest/index.html
import pytest
from PyQt5 import QtCore
from pytestqt.qt_compat import qt_api

import unittest, sys
from PyQt5 import QtGui, QtWidgets, QtTest


def test_isWidgetType(qtbot):
	# confirms FloatingPointController inherits from QWidget: class FloatingPointController(QWidget):
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.isWidgetType() ", window.isWidgetType()) # gives True
	assert window.isWidgetType()

	window.close()


def test_has_main_form(qtbot):
	# confirms FloatingPointController has an item main_form
	# which represents the form in the FloatingPointController
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	mf = [i for i in dir(window) if i == "main_form"]
	print("window ", mf)
	assert mf == ["main_form"]

	window.close()


def test_has_all_components(qtbot):
	# confirms if FloatingPointController's main_form has all necessary components
	# all items are added to the verticalLayout which is the layout of choice of the form itself
	# a vertical spacer item is also in verticalLayout but not queryable here
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	a = set()
	a.update([i for i in dir(window.main_form)])
	viewSet0 = {"button_new", "button_remove", "verticalLayout", "horizontalLayout"}
	assert viewSet0.issubset(a)

	a = set()
	a.update([i for i in dir(window.main_form.verticalLayout)])
	viewSet0 = {"spacerItem"}
	assert viewSet0.issubset(a)

	window.close()

def test_window_layout(qtbot):
	# checks if the form itself has the layout parameter vertical Layout QVBoxLayout checked
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.layout", window.layout())
	assert type(window.layout()) == QtWidgets.QVBoxLayout

	window.close()


def test_not_resizeable_0(qtbot):
	#checks if form is really set on fixed size
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	# test not resizeable
	assert window.main_form.verticalLayout.sizeConstraint() == QtWidgets.QLayout.SetFixedSize  

	print("verticalLayout.layoutSizeConstraint() ", window.main_form.verticalLayout.sizeConstraint()) # 3
	assert window.main_form.verticalLayout.sizeConstraint() == 3 
	# 3 means verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
	assert 3 == QtWidgets.QLayout.SetFixedSize 

	window.close()


def test_not_resizeable_1(qtbot):
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.size before ", window.size())
	sizeBefore = window.size()
	window.resize(500, 400)
	print("window.size after ", window.size())
	assert window.size() == sizeBefore #QtCore.QSize(500, 400) 

	window.close()


def test_size(qtbot):
	# checks if the nonresizeable window shrinks to is "fixed" size 
	# which is independent of its size of example 400*300 in the designer 
	# because fixed size shrinks the size 
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.geometry ", window.geometry())
	print("window.size ", window.size())
	assert window.size() == QtCore.QSize(322, 245) 

	window.close()


def test_horizontalLayout(qtbot):
	# checks if horizontal layout is part of, or added to, the vertical layout
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.main_form.verticalLayout.children ", type(window.main_form.verticalLayout.children()))
	assert type(window.main_form.verticalLayout.children()[0]) == QtWidgets.QHBoxLayout

	window.close()


def test_verticalLayout(qtbot):
	# checks that vertical layout covers the whole window size,
	# which means vertical layout is an adjective of the form itself
	# selected  by right click on the form -> layout
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.main_form.verticalLayout.geometry() ", window.main_form.verticalLayout.geometry())
	assert window.main_form.verticalLayout.geometry() == QtCore.QRect(0, 0, 322, 245)

	window.close()


def test_button_size(qtbot):
	# checks button_new to be on bottom left of the form
	# button is added as a widget to vertical layout of the form
	# checks button_remove to be on bottom right of the form
	assert qt_api.QApplication.instance() is not None
	window = FloatingPointController()

	window.show()
	qtbot.waitForWindowShown(window)
	assert window.isVisible()

	print("window.main_form.button_new.geometry() ", window.main_form.button_new.geometry())
	assert window.main_form.button_new.geometry() == QtCore.QRect(11, 211, 147, 23)

	print("window.main_form.button_remove.geometry() ", window.main_form.button_remove.geometry())
	assert window.main_form.button_remove.geometry() == QtCore.QRect(164, 211, 147, 23)

	window.close()

