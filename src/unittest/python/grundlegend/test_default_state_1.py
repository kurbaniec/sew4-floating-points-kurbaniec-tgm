#from floatingpoints
from floating_points_fixed_controller import FloatingPointController

# https://pytest-qt.readthedocs.io/en/latest/index.html
import pytest
from PyQt5 import QtCore
from pytestqt.qt_compat import qt_api
import psutil, time


def test_methods_exist_in_controller1(qtbot):
    # checks if controller has all requested methods

    cla = set()
    cla.update(v for v in vars(FloatingPointController)) # gets methods of controller class 

    # required methods in controller class
    meth = {"__init__", "new_point", "remove_point", "paintEvent", "draw_points", "closeEvent", "refresh_loop"}

    # ok if those methods exist in controller class
    assert meth.issubset(cla)


def test_controller_has_view(qtbot):
    # checks if controller has view called "main_form"
    assert qt_api.QApplication.instance() is not None

    w = FloatingPointController()
    w.show()

    # gets elements of controller
    x = set()
    x.update([i for i in dir(w)])

    viewSet = {"main_form"} #the name of the view

    # ok if the main_form exists in controller
    assert viewSet.issubset(x)

    w.close()


def test_view_elements_not_in_controller(qtbot):
    # checks if "button_new" and "button_remove" exist in view-file
    # checks if methods "setupUi", "retranslateUi" exist in view-file
    assert qt_api.QApplication.instance() is not None
    w = FloatingPointController()
    w.show()

    a = set()
    a.update([i for i in dir(w.main_form)])
 
    viewSet0 = {"button_new", "button_remove", "setupUi", "retranslateUi"}
    assert viewSet0.issubset(a)

    w.close()

    
def test_window_title(qtbot):
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()

    assert window.windowTitle() == "My Floating Points"
    window.close()


def test_labelButtonNew(qtbot):
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()
    assert window.main_form.button_new.text() == "New Point"


def test_labelButtonRemove(qtbot):
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()

    window.show()
    qtbot.waitForWindowShown(window)
    assert window.isVisible()
    assert window.main_form.button_remove.text() == "Remove Last Point"

    
def test_processes_added(qtbot):
    # adds 4 points and checks if this created 4 new processes
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    # qtbot.waitForWindowShown(window)
    assert window.isVisible()

    bef = set()
    for proc in psutil.process_iter():
        bef.add(proc.pid)
    # print("bef set ", len(bef))

    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
 
    time.sleep(0.025)    

    aft = set()
    for proc in psutil.process_iter():
        aft.add(proc.pid)
    # print("after set ", len(aft))

    # print(aft - bef)
    # print(aft ^ bef)

    window.close()
    assert len(aft) - len(bef) == 4


def test_clean_close(qtbot):
    # Adds 4 points, set safe_close = True and checks if the 4 processes disappeared
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    # qtbot.waitForWindowShown(window)
    assert window.isVisible()

    bef = set()
    for proc in psutil.process_iter():
        bef.add(proc.pid)     

    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
 
    time.sleep(0.025)    

    aft = set()
    for proc in psutil.process_iter():
        aft.add(proc.pid) 

    assert len(aft) - len(bef) == 4

    for pointPositions in window.point_positions:
        pointPositions[2] = False
    window.safe_close = True # may be added
    time.sleep(1.025)    

    aftAft = set()
    aftAft = aft - bef
    for p in aftAft:
        print(p)
        assert psutil.pid_exists(p) == False
        
    window.close() 


def test_processes_app_closed(qtbot):
    # Adds 4 points, closes the app and checks if the 4 processes disappeared
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    # qtbot.waitForWindowShown(window)
    assert window.isVisible()

    bef = set()
    for proc in psutil.process_iter():
        bef.add(proc.pid)     

    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
 
    time.sleep(0.025)    

    aft = set()
    for proc in psutil.process_iter():
        aft.add(proc.pid) 

    assert len(aft) - len(bef) == 4

    window.close()
    # qt_api.QApplication.quit() # may be added
    time.sleep(1.025)    

    aftAft = set()
    aftAft = aft - bef
    for p in aftAft:
        print(p)
        assert psutil.pid_exists(p) == False


def test_processes_removed(qtbot):
    # Adds 4 new points, checks the PIDs, removes 4 points, checks if PIDs disappeared
    assert qt_api.QApplication.instance() is not None
    window = FloatingPointController()
    window.show()
    # qtbot.waitForWindowShown(window)
    assert window.isVisible()

    bef = set()
    for proc in psutil.process_iter():
        bef.add(proc.pid) #proc.name())        
    # print("bef set ", len(bef))

    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_new, QtCore.Qt.LeftButton)
 
    time.sleep(0.025)    

    aft = set()
    for proc in psutil.process_iter():
        aft.add(proc.pid)

    assert len(aft) - len(bef) == 4

    qtbot.mouseClick(window.main_form.button_remove, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_remove, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_remove, QtCore.Qt.LeftButton)
    qtbot.mouseClick(window.main_form.button_remove, QtCore.Qt.LeftButton)

    time.sleep(1.025)    

    aftAft = set()
    aftAft = aft - bef

    for p in aftAft:
        print(p)
        assert psutil.pid_exists(p) == False

    window.close()

