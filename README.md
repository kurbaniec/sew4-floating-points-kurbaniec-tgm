# "Floating Points"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung
Um meine View anzeigen zu lassen, musste ich ein paar Schritte befolgen. <br>
Zuerst musste ich im Konstruktor ein neues Attribut erstellen, in der ich die View initialisiert habe 
und anschließend mit setupUi aufgebaut habe. <br>
Doch leider bekam ich beim Ausführen des Controllers keine GUI angezeigt, nur ganz kurz flackert beim 
Starten was ein Fenster schwarz auf. <br>
Zum Glück fand ich im Internet eine [Lösung](https://stackoverflow.com/questions/31815399/pyqt-qwidget-window-closes-immediately-upon-showing) für das Problem, denn wie sich herausstellte beendete der Befehl 
*sys.exit()* sofort die Applikation mit der GUI, obwohl man als Benutzer das Fenster der GUI nicht geschlossen hat, 
dies war dem Kommando komplett egal. Darum muss man den exit Befehl sagen, dass er das Programm erst schließen soll, 
wenn der User die GUI schließt. Dies kann mit *sys.exit(app.exec_())* realisiert werden. <br>
Nachdem dies funktionierte, musste ich irgendwie herausfinden, wie der generelle Ablauf des Programmes aussehen
sollte. Die Methodennamen halfen, sie erinnerten mich an Swing von Java, aber ich konnte mir nicht ganz den 
Programmverlauf vorstellen. Dazu fand ich eine aber ein ganz gute Anleitung, die diesen erklärte. Ich
adaptierte einiges für mein Programm und siehe da ich konnte schon Elemente zeichnen. <br>
Bisschen Probleme hatte ich mit der parallen Ausführung von Prozessen, doch nachdem ich mir die Multiprocessing-API
angeschaut habe, verstand ich, dass ich diese Prozesse mit `.start()` starten soll, um die diese quasi "gleichzeit" 
laufen zu lassen. Auch wurde `.join()` und `.close` erwähnt. Mit dem ersten Methode wartet man bis der Prozess
seine Arbeit erledigt hat, der zweite schließt bzw. beendet ihn sauber. <br>
Nachdem das Programm grundlegend funktionerte, begann ich mit der erweiterten Kompetenz. Dabei musste eine 
resizable GUI erstellt werden, also musste ich wieder mit den *Designer* arbeiten. Dort entdeckte ich nach einiger Zeit Trial-and-Error
den Vertical Spacer und benutzte ihn, gemeinsam mit Änderungen von ein paar Einstellungen (keine fixed Size), um eine
skalierbare Oberfläche zu erstellen. Was knifflig war, war die Erstellung der Elemente, (Textfeld, Buttons, etc.) für den
EK-Teil, weil sie ein zusätzliches Layout brauchten was wiederum mit dem alten in einen gemeinsamen Layout platziert werden musste.
Doch nachdem sie erstellt worden war, stellte ich im *tox*-File die View auf die Resizable um und begann mit
der Implemenitierung ihrer Funktionen im Code.<br>
Für diese musste ich den Punkten, die aus einem Multiprocessing-Array bestanden, zwei weitere Eigenschaften anhängen: Größe und Farbe.
Das Einfärben der Punkte und die Veränderung der Punkte erwies sich nicht wirklich als schwierig, man muss bei Painter nur die
`.setPen(color)`-Methode für das Einfärben verwenden, die Größe wird bei der Draw-Methode einfach als dritter und vierter
Parameter übergeben.<br>
Die Umsetzung der Skalierbarkeit war eher das Problem. Dazu habe ich zuerst zwei Multiprocessing-Values benutzt, um die Größen,
also die Höhe und Breite des Fensters zu speichern und diese auch für alle Prozesse zugänglich zu machen. Anfangs hatte ich
ein Logikproblem, den immer bei der Übergabe dieser, wurde der Datentyp von Values auf int geändert. Dies kam daher, dass ich bei
der Initalisierung nicht `.value` verwendet habe und Python einfach den Datentyp überschrieb. Nachdem ich meinen Fehler einsah
realisierte ich noch ein *resizeEvent* implementier, sodass bei Änderung der Fenstermaße die Multiprocessing-Values
aktualisiert werrden. Ein Problem aber war, dass wenn ich zu stark die Fenstermaße veränderte, so dass
die Punkte größer als beispielweise die Höhe waren, wurden diese durch den Algortihmus von *living_point* in einer
*Infinite* Position gezeichnet, was zu einem fehlerhaften Paint-Event führte und in einer Exception mündete.
Deshalb habe ich beim Initalisieren der GUI eine mindest Höhe mit `self.setMinimumHeight(160)` verpasst, sowie *living_point* 
adaptiert, dass Punkte außerhalb der GUI langsam in diese zurückgedrängt werden solllen, damit sie wieder 
korrekt gezeichnet werden. <br>


#### Test-Cases
Diese basieren nicht auf Tox und müssen manuel zu den Source-Files verlegt werden und dort mit `pytest` in der Command-Line
ausgeführt werden. Die Test-Cases beziehen sich aber stark auf Attribute, die ich beispielsweise anderes
bennant haben, weil nichts vorgegeben wurde, so dass viele der Cases einfach dadurch schon failen.   


## Quellen
Qt-Window: https://stackoverflow.com/questions/31815399/pyqt-qwidget-window-closes-immediately-upon-showing <br>
Python-Value: https://eli.thegreenplace.net/2012/01/04/shared-counter-with-pythons-multiprocessing <br>
Python-Multiprocessing: https://docs.python.org/2/library/multiprocessing.html <br>
Qt-Farben: http://doc.qt.io/archives/qt-4.8/qcolor.html <br>
Qt-Combobox: https://stackoverflow.com/questions/44707794/pyqt-combo-box-change-value-of-a-label <br>
Genereller Programmablauf: http://zetcode.com/gui/pyqt4/drawing/ <br>
Genereller Programmablauf: http://zetcode.com/gui/pyqt5/painting/